# Stylo
## A user friendly text editor for humanities scholars
<small>Antoine Fauchié – [antoine.fauchie@umontreal.ca](mailto:antoine.fauchie@umontreal.ca)<br>
Canada Research Chair on Digital Textualities – [ecrituresnumeriques.ca](https://ecrituresnumeriques.ca/)</small>

<small>[https://ecrinum.gitpages.huma-num.fr/stylo/presentations/fosdem-2020](https://ecrinum.gitpages.huma-num.fr/stylo/presentations/fosdem-2020)</small>

<img src="images/logo.svg" class="logo"/> <img src="images/logo-udem.png" class="logo2"/>

===n===
First of all, thank you to the "Open Research Tools & Technologies" team, I'm honored to be part of this fantastic gang!
My name is Antoine Fauchié, I'm an old new PhD student (I'm 36), I'm from Montréal but I'm french (you have already figured it out with my accent).
I'm going to talk about writing tools, publication toolchain and freedom.

My presentation is already available online, I’m using Reveal.js so you can watch my notes with the "S" key.

===r===

## Plan

1. Context<!-- .element: class="fragment" -->
2. Tools of pain<!-- .element: class="fragment" -->
3. What we want<!-- .element: class="fragment" -->
4. Existing effective tools<!-- .element: class="fragment" -->
5. Stylo<!-- .element: class="fragment" -->

===n===
So you can understand the team's starting point, I have to explain at first the context.

Then I will present the tools that are used by the majority in the scientific community, and I call them "Tools of pain".
Note that I'm not going to say burn all the word processors, but a little bit.

The third part is about what we need for doing our job, as researchers: such as comfort, I like the new buzzword in the development community: DX (like UX) for developer experience.

And finally I will present about great, beautiful, powerful and free tools.

And of course I'm going to introduce Stylo, a gorgeous user friendly text editor for humanities scholars.

===r===

## 1. Context

- academic publishing<!-- .element: class="fragment" -->
- humanities scholars<!-- .element: class="fragment" -->
- digital world<!-- .element: class="fragment" -->

===d===

### 1.1. Context: academic publishing
Specific requirements:

- content structuration: semantic, citations, footnotes<!-- .element: class="fragment" -->
- researchers love PDF, but we need the Web<!-- .element: class="fragment" -->
- it's all about XML<!-- .element: class="fragment" -->

===n===
Publishing ecosystem is not uniform, and academic publishing have specific requirements.

Firts fo all, it's about semantic text, with current footnotes, citations and bibliographic data.

In the publishing ecosystem we love PDF, but we need the web.
This is a fact. Even if, sometimes, we would like to see the frozen PDF format disappear...

And it's all about XML. In academic publishing XML is everywhere, it's the source of all joy and depression. Joy, because we can do everything with XML. Depression, because mostly we create a big technical debt (never forget that DOCX is actually XML).

===d===

### 1.2. Context: humanities scholars
It's all about text!<!-- .element: class="fragment" -->

===n===
I'm aware that we all came from a wide variety of fields.
So I need to clarify specificities for the humanities: it's about text! And sometimes data.
If the text is the horizon of the research, we must handle it with care.

===d===

### 1.3. Context: digital world
The need for structured content, metadata, collaborative tools.<!-- .element: class="fragment" -->

===n===
Saying as an evidence, our world is now a digital one. Even if you don't want to be connected, or don’t have an _academia_ profile, a Twitter dependency or your own website.
As researchers we can’t write a paper without metadata, structured content, bibliographic database or collaborative tools.

And in the future, we will have to transform our research into available data, if we haven't already done so.

===r===

## 2. Tools of pain

- Word/LaTeX duality<!-- .element: class="fragment" -->
- Google Docs limitation<!-- .element: class="fragment" -->

===d===

### 2.1. Tools of pain: Microsoft Word
A realistic (and depressing) domination.

===n===
Microsoft Word is a proprietary software.
The major issue with Word (and with LibreOffice Writer) is the confusion between content and form, or structure and graphic rendering.

Most of the time, Microsoft Word's users think that graphic rendering _is_ the structure.
But a word in bold is not obviously an emphasis, although an emphasis can be in bold.

When an publisher requires a stylesheet or give some structure guidelines, it's not easy for the writers to understand and apply it.

===d===

<!-- .slide: data-background="images/word.png" -->

===n===
Those are the files that constitute a Word document, when you typed "hello" and saved.
It's quite tricky, just for **one** word.
Thanks to the agency Information Architect for this crappy picture.

===d===

### 2.2. Tools of pain: LaTeX
Powerful<!-- .element: class="fragment" -->, but too complicated.

===n===
Great online tools like Overleaf reduce the complexity of LaTeX, but it's still a very restrictive tool.
We cannot ask the writers to not forget to close a bracket. Or to learn a new syntax

===d===

### 2.3. Tools of pain: Google Docs
Connexion dependency, proprietary cloud, so bad for our privacy, semantically limited.

(Google Docs is the new Microsoft Word)<!-- .element: class="fragment" -->

===n===
Google Docs is becoming very common, with a lot of constraints.

===r===

## 3. What we want

- semantic content, data<!-- .element: class="fragment" -->
- manifold artefacts<!-- .element: class="fragment" -->
- single source publishing<!-- .element: class="fragment" -->
- easy interfaces<!-- .element: class="fragment" -->

===d===

### 3.1. What we want: semantic content
Give meaning to any part of text.

===n===
As researchers, we don't write text only.
We both inscribe and structure.
We need tools that allow to do that both.

And it's not just about bold and italic.

===d===

### 3.2. What we want: manifold artefacts

- PDF<!-- .element: class="fragment" -->
- XML<!-- .element: class="fragment" -->
- HTML/web<!-- .element: class="fragment" -->
- EPUB ?<!-- .element: class="fragment" -->
- etc.<!-- .element: class="fragment" -->

===n===

===d===

### 3.3. What we want: single source publishing
Manifold artefacts<!-- .element: class="fragment" -->, one source!

===n===
Researchers and editors can't work on the same version of the document:

- Word version (author)
- InDesign version (graphic designer)
- XML version (editor/publisher)

It's hard to maintain 3 files version of **one** document.

===d===

<img src="images/single-source-publishing.png" width="70%" class="ssp"/>


===d===

### 3.4. What we want: usable and efficient interfaces
Easy and powerful.

===n===
Interfaces like Google Docs introduce an attempt of efficience interfaces.

We need différent levels of editing :

- author
- editor
- data manager

===r===

## 4. Existing effective tools

- Markdown<!-- .element: class="fragment" -->
- CSL<!-- .element: class="fragment" -->
- Pandoc<!-- .element: class="fragment" -->

===n===
Just a quick view of 3 tools that allows:

- structuration
- dynamic bibliographic data
- manifold artefacts with one and unique source

===d===

### 4.1 Existing effective tools: Markdown
Structured content, easily<!-- .element: class="fragment" --> (understandable for both humans and programs)

===n===
Markdown is a great compromise between interoperability and simple markup.

And Markdown is extensible with HTML classes!

===d===

<!-- .slide: data-background="images/markdown.png" -->

===n===
Here an example of the markup and the rendering.

===d===

### 4.1 Existing effective tools: CSL
Structured citations and bibliographies<!-- .element: class="fragment" -->, with any bibliographic styles.

===d===

### 4.1 Existing effective tools: Pandoc
Transform, convert!

===n===
From any source to a lot of formats.

But you have to use a damn terminal.

===r===

## 5. Stylo
A modular and WYSIWYM text editor for scientific writing and publishing.

===n===
Now it's time to show you a tool as an answer of what researchers and editors want (in human sciences field).
Stylo is a modular and WYSIWYM (**What You See Is What You Mean**, and not What You See Is What You Get) text editor for scientific writing and publishing.

===d===

### 5.1. Stylo: a modular solution (for scientific writing)

- text editor with markup: **Markdown**
- bibliographic data: **BibTeX**, **Zotero API**
- metadata management: **YAML** (+ DC, RDFa, Foaf)
- plateform: MongoDB (database), GraphQL (backend), ReactJS (frontend)

===n===
Brings Markdown, CSL, Pandoc, and a usable interface together.
Using Hypothes.is for reviewing.

===d===

<!-- .slide: data-background="images/stylo-naked.png" -->

===n===
As you can see, Stylo is composed of three areas:

1. content manager in the center
2. versioning (save), preview, export, bibliography and informations on the left
3. metadata on the right (with 3 levels of management)

===d===

<!-- .slide: data-background="images/stylo-versions.png" -->

===n===
Versions.

===d===

<!-- .slide: data-background="images/stylo-toc.png" -->

===n===
Table of contents.

===d===

<!-- .slide: data-background="images/stylo-bibliography.png" -->

===n===
Bibliography imported from a BibTeX file or from a collection of a Zotero shared group.

===d===

<!-- .slide: data-background="images/stylo-editor-mode.png" -->

===n===
Editor mode for the metadata.

===d===

<!-- .slide: data-background="images/stylo-raw-mode.png" -->

===n===
Here we can add new fields as you want.

===d===

<!-- .slide: data-background="images/stylo-preview.png" -->

===n===
The preview is just an HTML export with a default style sheet.

On the preview it's possible to annotate the preview, for reviewing a paper for example.
We use Hypothes.is, a great web annotation service.
Hypothes.is allow to use private group for private annotations.


===d===

<!-- .slide: data-background="images/stylo-export.png" -->

===n===
And it's possible to export in many format.
The files export use 3 sort of document:

1. Markdown for the content
2. BibTeX for the bibliographic references
3. YAML for the metadata
4. For the XML we have to create a XSLT stylesheet based on the schema of the requester

===d===

### 5.2. Stylo: change the entire digital editorial chain of scholarly journals
From Stylo to XML, PDF, HTML.

===n===
We have an import tool, for Microsoft Word purpose.

===d===

<!-- .slide: data-background="images/stylo-nouvelles-vues.png" -->

===n===
As you can see here is the _Nouvelles vues_ journal:

- work editing in Stylo
- HTML export for there website
- PDF export for the _Nouvelles vues_ website and Érudit
- XML export for Érudit
- **one source**

===d===

## 5.3. Stylo: a (replaceable) piece of a new (digital by fact) world
Stylo's life in 3 acts:

1. a proof of concept (2018)<!-- .element: class="fragment" -->
2. a unique tool to help scientific publishing (2019)<!-- .element: class="fragment" -->
3. a piece of other tools: Nakala, Isidore.science (2020)<!-- .element: class="fragment" -->

===d===

## 5.4. Stylo: resources

- the online editor:  
[https://stylo.ecrituresnumeriques.ca/](https://stylo.ecrituresnumeriques.ca/)
- the documentation:  
[https://stylo-doc.ecrituresnumeriques.ca/](https://stylo-doc.ecrituresnumeriques.ca/)
- the source code:  
[https://github.com/EcrituresNumeriques/stylo/](https://github.com/EcrituresNumeriques/stylo/)

And a team: Nicolas, Marcello, Arthur, Margot, Antoine, and many people.<!-- .element: class="fragment" -->

===r===

## Contact
<small>Antoine Fauchié – [antoine.fauchie@umontreal.ca](mailto:antoine.fauchie@umontreal.ca)  
Canada Research Chair on Digital Textualities – [ecrituresnumeriques.ca](https://ecrituresnumeriques.ca/)</small>

Test Stylo now → [https://stylo.ecrituresnumeriques.ca/](https://stylo.ecrituresnumeriques.ca/)

<small><br><br>Watch again:  
[https://ecrinum.gitpages.huma-num.fr/stylo/presentations/fosdem-2020/](https://ecrinum.gitpages.huma-num.fr/stylo/presentations/fosdem-2020/)</small>
