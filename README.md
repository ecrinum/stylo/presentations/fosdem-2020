# Stylo: a user friendly text editor for humanities scholars
Presentation during the [Open Research Tools and Technologies devroom](https://research-fosdem.github.io/) at [the FOSDEM 2020](https://fosdem.org/2020/).
[Page of the schedule](https://fosdem.org/2020/schedule/event/open_research_stylo/).

Presentation can be watch on: [https://ecrinum.gitpages.huma-num.fr/stylo/presentations/fosdem-2020/](https://ecrinum.gitpages.huma-num.fr/stylo/presentations/fosdem-2020/).

## Abstract
As an editor for WYSIWYM text, Stylo is designed to change the entire digital editorial chain of scholarly journals the field of human sciences.

Stylo ([https://stylo.ecrituresnumeriques.ca](https://stylo.ecrituresnumeriques.ca)) is designed to simplify the writing and editing of scientific articles in the humanities and social sciences. It is intended for authors and publishers engaged in high quality scientific publishing. Although the structuring of documents is fundamental for digital distribution, this aspect is currently delayed until the end of the editorial process. This task should, however, be undertaken early on in the process; it must be considered by the author himself. The philosophy behind Stylo consists in returning the task of managing the publication markup to researchers. This repositioning of tasks relating to the editorial process relies on the author’s semantic rather than graphic skills.

This lightning talk will be the opportunity to present this tool and several publishing projects realized with Stylo.

## Plan

1. Context: academic publishing, humanities scholars, digital world
  - academic publishing: a specific requirement in the publishing ecosystem (we love PDF but we need the web)
  - humanities scholars: it's about text (academic literary)
  - digital world: the need for structured content, etc.
2. Tools of pain: Words/LaTeX duality, Google Docs limitation
  - Words: a realistic (and depressing) domination
  - LaTeX: too smart (for me)
  - Google Docs: simple and therefore limited
3. What we want: semantic content/data, manifold artefacts (+ easy interfaces)
4. Existing effective tools: Markdown, CSL, Pandoc
5. Stylo
